#include <iostream>
#include "shared/SinglyLinkedList.hpp"
using namespace std;


void printSinglyLinkedList(SinglyLinkedList* singlyLinkedList) {
	for (int i = 0; i < singlyLinkedList->sizeOf(); i++) {
		cout << singlyLinkedList->get(i) << " ";
	}
	cout << endl;
}

void deleteDuplicates(SinglyLinkedList* singlyLinkedList) {
    auto current = singlyLinkedList->getHead();
    while (current->getNext()) {
        if (current->getValue() == current->getNext()->getValue()) {
            current->setNext(current->getNext()->getNext());
        } else {
            current = current->getNext();
        }
    }
}

int main() {
    SinglyLinkedList* lst = new SinglyLinkedList();
    int n, x;
    cin >> n;
    for (int i = 0; i < n; i++) {
        cin >> x;
        lst->append(x);
    }
    
    deleteDuplicates(lst);

    printSinglyLinkedList(lst);

    return 0;
}
