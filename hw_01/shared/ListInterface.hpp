#pragma once


class ListInterface {
public:
	virtual void append(int x) = 0;
	virtual int get(int index) = 0;
	virtual int sizeOf() = 0;
	virtual void insert(int element, int index) = 0;
	virtual int remove(int index) = 0;
	virtual bool find(int x) = 0;
	virtual int count(int x) = 0;
	virtual void reverse() = 0;
};
