#include "SinglyLinkedList.hpp"
#include <cstdlib>


SinglyLinkedList::SinglyLinkedList(int* initArr, int initArrSize, bool isReverse) {
    this->head = new ListNode();
    ListNode* current = this->head;
    for (int i = 0; i < initArrSize; i++) {
        current->setValue(initArr[!isReverse ? i : initArrSize - i - 1]);
        current = current->getNext();
    }
}

SinglyLinkedList::SinglyLinkedList() {}

SinglyLinkedList::~SinglyLinkedList() {
    delete this->head;
}

void SinglyLinkedList::append(int x) {
    if (this->head) {
        ListNode* current = this->head;
        while (current->getNext()) {
            current = current->getNext();
        }
        current->setNext(new ListNode());
        current->getNext()->setValue(x);
    } else {
        this->head = new ListNode();
        this->head->setValue(x);
    }
}

int SinglyLinkedList::get(int index) {
    if (!this->head) return NULL;
    ListNode* current = this->head;
    for (int i = 0; i < index; i++) {
        current = current->getNext();
    }
    return current->getValue();
}

int SinglyLinkedList::sizeOf() {
    ListNode* current = this->head;
    int size;
    for (size = 0; current; size++) {
        current = current->getNext();
    }
    return size;
}

void SinglyLinkedList::insert(int element, int index) {
    if (index == 0) {
        ListNode* next = this->head;
        this->head = new ListNode();
        this->head->setValue(element);
        if (this->head) this->head->setNext(next);
        return;
    }
    ListNode* current = this->head;
    for (int i = 0; i < index - 1; i++) {
        current = current->getNext();
    }
    ListNode* nextCopy = current->getNext();
    ListNode* newNext = new ListNode();
    current->setNext(newNext);
    newNext->setValue(element);
    newNext->setNext(nextCopy);
}

int SinglyLinkedList::remove(int index) {
	if (index == 0) {
		int valueCopy = this->head->getValue();
        ListNode* headNext = this->head->getNext();
		delete this->head;
        this->head = this->head->getNext() ? headNext : nullptr;
        return valueCopy;
	}
	ListNode* current = this->head;
    for (int i = 0; i < index - 1; i++) {
        current = current->getNext();
    }
	// current->getNext() must be removed
    int valueCopy = current->getNext()->getValue();
    if (current->getNext()->getNext()) {
        current->setNext(current->getNext()->getNext());
    } else {
        delete current->getNext();
        current->setNext(nullptr);
    }
    return valueCopy;
}

bool SinglyLinkedList::find(int x) {
    ListNode* current = this->head;
    while (current) {
        if (current->getValue() == x) return true;
        current = current->getNext();
    }
    return false;
}

int SinglyLinkedList::count(int x) {
    int count = 0;
    ListNode* current = this->head;
    while (current) {
        if (current->getValue() == x) count++;
        current = current->getNext();
    }
    return count;
}

void SinglyLinkedList::reverse() {
    ListNode* newHead = nullptr;
    ListNode* saveNext;
    ListNode* current = this->head;
    while (current) {
        saveNext = current->getNext();
        current->setNext(newHead);
        newHead = current;
        current = saveNext;
    }
    this->head = newHead;
}
