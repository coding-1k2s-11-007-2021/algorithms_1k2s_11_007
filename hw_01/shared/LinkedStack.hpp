#pragma once
#include "StackInterface.hpp"
#include "SinglyLinkedList.hpp"


class LinkedStack : public StackInterface {
private:
    SinglyLinkedList* lst;

public:
    LinkedStack();

    ~LinkedStack();

    void push(int x);
    int pop();
    bool isEmpty();
    int peek();
};
