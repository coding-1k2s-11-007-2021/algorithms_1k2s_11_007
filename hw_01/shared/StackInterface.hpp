#pragma once


class StackInterface {
public:
    virtual void push(int x) = 0;
    virtual int pop() = 0;
    virtual bool isEmpty() = 0;
    virtual int peek() = 0;    
};
