#include "ArrayStack.hpp"


ArrayStack::ArrayStack() {
    this->lst = new ArrayList();
}

ArrayStack::~ArrayStack() {
    delete this->lst;
}

void ArrayStack::push(int x) {
    this->lst->append(x);
}

int ArrayStack::pop() {
    return this->lst->remove(this->lst->sizeOf() - 1);
}

bool ArrayStack::isEmpty() {
    return this->lst->sizeOf() == 0;
}

int ArrayStack::peek() {
    return this->lst->get(this->lst->sizeOf() - 1);
}
