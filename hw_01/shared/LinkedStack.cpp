#include "LinkedStack.hpp"
#include <cstdlib>


LinkedStack::LinkedStack() {
    this->lst = new SinglyLinkedList();
}

LinkedStack::~LinkedStack() {
    delete this->lst;
}

void LinkedStack::push(int x) {
    this->lst->insert(x, 0);
}

int LinkedStack::pop() {
    return this->lst->remove(0);
}

bool LinkedStack::isEmpty() {
    return this->lst->get(0) == NULL;
}

int LinkedStack::peek() {
    return this->lst->get(0);
}
