#pragma once
#include "ListInterface.hpp"


class ListNode {
private:
	int value;
	ListNode* next = nullptr;

public:
	int getValue() { return this->value; }
	void setValue(int x) { this->value = x; }
	ListNode* getNext() { return this->next; }
	void setNext(ListNode* x) { this->next = x; }
};


class SinglyLinkedList : public ListInterface {
private:
	ListNode* head = nullptr;

public:
	SinglyLinkedList(int* initArr, int initArrSize, bool isReverse);
	SinglyLinkedList();

	~SinglyLinkedList();

	ListNode* getHead() { return this->head; }

	void append(int x);
	int get(int index);
	int sizeOf();
	void insert(int element, int index);
	int remove(int index);
	bool find(int x);
	int count(int x);
	void reverse();
};
