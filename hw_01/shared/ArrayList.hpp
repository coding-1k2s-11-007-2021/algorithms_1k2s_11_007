#pragma once
#include "ListInterface.hpp"


class ArrayList : public ListInterface {
private:
	int* arr;
	int size;
	int capacity;

	void extend();

public:
	ArrayList(int* initArr, int initArrSize, bool isReverse);
	ArrayList(int initArrSize);
	ArrayList();

	~ArrayList();

	void append(int x);
	int get(int index);
	int sizeOf();
	void insert(int element, int index);
	int remove(int index);
	bool find(int x);
	int count(int x);
	void reverse();
};
