#pragma once
#include "StackInterface.hpp"
#include "ArrayList.hpp"


class ArrayStack : public StackInterface {
private:
    ArrayList* lst;

public:
    ArrayStack();

    ~ArrayStack();

    void push(int x);
    int pop();
    bool isEmpty();
    int peek();
};
