#include <algorithm>  // for std::swap
#include "ArrayList.hpp"


ArrayList::ArrayList(int* initArr, int initArrSize, bool isReverse): ArrayList(initArrSize) {
	for (int i = 0; i < initArrSize; i++) {
		this->arr[i] = initArr[isReverse ? initArrSize - i - 1 : i];
	}
}

ArrayList::ArrayList(int initArrSize) {
	this->size = initArrSize;
	this->capacity = initArrSize * 2;
	this->arr = new int[initArrSize * 2];
}

ArrayList::ArrayList() {
	this->size = 0;
	this->capacity = 5;
	this->arr = new int[5];
}

ArrayList::~ArrayList() {
	delete[] this->arr;
}

int ArrayList::get(int index) {
	return this->arr[index];
}

int ArrayList::sizeOf() {
	return this->size;
}

void ArrayList::append(int x) {
	this->insert(x, this->size);
}

void ArrayList::extend() {
	if (this->size == this->capacity) {
        this->capacity *= 2;
        int* newArr = new int[this->capacity];
        for (int i = 0; i < this->size; i++) {
            newArr[i] = this->arr[i];
        }
        delete [] this->arr;
        this->arr = newArr;
    }
}

void ArrayList::insert(int element, int index) {
	this->extend();
	this->size += 1;
	for (int i = this->size - 1; i > index; i--) {
		this->arr[i] = this->arr[i - 1];
	}
	this->arr[index] = element;
}

int ArrayList::remove(int index) {
    this->size -= 1;
    int removed_elem = this->arr[index];
    for (int i = index; i < this->size; i++) {
        this->arr[i] = arr[i + 1];
    }
    return removed_elem;
}

bool ArrayList::find(int x) {
    for (int i = 0; i < this->size; i++) {
        if (this->arr[i] == x)
            return true;
    }
    return false;
}

int ArrayList::count(int x) {
    int count = 0;
    for (int i = 0; i < this->size; i++) {
        if (this->arr[i] == x)
            count++;
    }
    return count;
}

void ArrayList::reverse() {
	for (int i = 0; i < this->size / 2; i++) {
		std::swap(this->arr[i], this->arr[this->size - 1 - i]);
	}
}
