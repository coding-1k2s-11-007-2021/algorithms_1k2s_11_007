#include <iostream>
#include "shared/ArrayList.hpp"
using namespace std;


void printArrayList(ArrayList *arrayList) {
	for (int i = 0; i < arrayList->sizeOf(); i++) {
		cout << arrayList->get(i) << " ";
	}
	cout << endl;
}

int main() {
	int n, countOfElemsInRow, curElem;
	ArrayList *lst = new ArrayList();
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> countOfElemsInRow;
		for (int j = 0; j < countOfElemsInRow; j++) {
			cin >> curElem;
			lst->append(curElem);
		}
	}
	printArrayList(lst);
	cout << lst->count(5) << endl;
	cout << boolalpha << lst->find(3) << endl;
	lst->remove(lst->get(0));
	lst->insert(3, lst->get(0));
	lst->reverse();
	printArrayList(lst);
	cout << lst->sizeOf() << endl;

	return 0;
}
