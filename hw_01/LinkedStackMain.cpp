#include <iostream>
#include "shared/LinkedStack.hpp"
using namespace std;


int main() {
    LinkedStack* linkedStack = new LinkedStack();
    int x, y;
    cin >> x >> y;
    linkedStack->push(x);
    linkedStack->push(y);
    cout << boolalpha << linkedStack->isEmpty() << endl;
    cout << linkedStack->peek() << endl;
    cout << linkedStack->pop() << endl;
    cout << boolalpha << linkedStack->isEmpty() << endl;
    cout << linkedStack->peek() << endl;
    cout << linkedStack->pop() << endl;
    cout << boolalpha << linkedStack->isEmpty() << endl;

    return 0;
}
