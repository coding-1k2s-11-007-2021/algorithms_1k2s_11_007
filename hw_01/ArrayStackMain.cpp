#include <iostream>
#include "shared/ArrayStack.hpp"
using namespace std;


int main() {
    ArrayStack* arrStack = new ArrayStack();
    int x, y;
    cin >> x >> y;
    arrStack->push(x);
    arrStack->push(y);
    cout << boolalpha << arrStack->isEmpty() << endl;
    cout << arrStack->peek() << endl;
    cout << arrStack->pop() << endl;
    cout << boolalpha << arrStack->isEmpty() << endl;
    cout << arrStack->peek() << endl;
    cout << arrStack->pop() << endl;
    cout << boolalpha << arrStack->isEmpty() << endl;

    delete arrStack;

    return 0;
}
