## Метод: сложность по времени, сложность по памяти

### **ArrayList:**
1. ArrayList(int* initArr, int initArrSize, bool isReverse): O(initArrSize), O(initArrSize)
2. ArrayList(int initArrSize): O(1), O(initArrSize)
3. ArrayList(): O(1), O(1)
4. append: в основном O(1), O(1); иногда O(size), O(capacity)
5. get: O(1), O(1)
6. sizeOf: O(1), O(1)
7. insert: в основном O(size), O(1); иногда O(size), O(capacity)
8. remove: O(size), O(1)
9. find: O(size), O(1)
10. count: O(size), O(1)
11. reverse: O(size), O(1)

### **SinglyLinkedList:**
1. SinglyLinkedList(int* initArr, int initArrSize, boolean isReverse): O(initArrSize), O(initArrSize)
2. SinglyLinkedList(): O(1), O(1)
3. append: O(n), O(1)
4. get: O(index), O(1)
5. sizeOf: O(n), O(1)
6. insert: O(index), O(1)
7. remove: O(index), O(1)
8. find: O(n), O(1)
9. count: O(n), O(1)
10. reverse: O(size), O(1)

### **ArrayStack:**
1. ArrayStack(): O(1), O(1)
2. push: в основном O(1), O(1); иногда O(size), O(capacity)
3. pop: O(1), O(1)
4. isEmpty: O(1), O(1)
5. peek: O(1), O(1)

### **LinkedStack:**
1. LinkedStack(): O(1), O(1)
2. push: O(1), O(1)
3. pop: O(1), O(1)
4. isEmpty: O(1), O(1)
5. peek: O(1), O(1)
