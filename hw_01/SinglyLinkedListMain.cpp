#include <iostream>
#include "shared/SinglyLinkedList.hpp"
using namespace std;


void printSinglyLinkedList(SinglyLinkedList* singlyLinkedList) {
	for (int i = 0; i < singlyLinkedList->sizeOf(); i++) {
		cout << singlyLinkedList->get(i) << " ";
	}
	cout << endl;
}

int main() {
	int n, countOfElemsInRow, curElem;
	SinglyLinkedList* lst = new SinglyLinkedList();
	cin >> n;
	for (int i = 0; i < n; i++) {
		cin >> countOfElemsInRow;
		for (int j = 0; j < countOfElemsInRow; j++) {
			cin >> curElem;
			lst->append(curElem);
		}
	}
	printSinglyLinkedList(lst);
	cout << lst->count(5) << endl;
	cout << boolalpha << lst->find(3) << endl;
	lst->remove(lst->get(0));
	lst->insert(3, lst->get(0));
	lst->reverse();
	printSinglyLinkedList(lst);
	cout << lst->sizeOf() << endl;

	return 0;
}
