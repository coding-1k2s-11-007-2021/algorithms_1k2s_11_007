#include <iostream>
#include <vector>


using namespace std;


class Person {
    private:
        int age;
        string name;
        vector<int> likedSongs;
        
    public:
        Person(string name, int age = 18) {
            this->name = name;
            this->age = age;
        }

        void appendLikedSongs(int* likedSongsArr, int likedSongsArrLen) {
            for (int i = 0; i < likedSongsArrLen; i++) {
                likedSongs.push_back(likedSongsArr[i]);
            }
        }

        void printInfo() {
            cout << "Name is " << this->name << endl;
            cout << "Age is " << this->age << endl;
            cout << "Liked songs are ";
            for (int n : this->likedSongs) {
                std::cout << n << " ";
            }
            cout << endl;
        }
};


int main() {
    Person* person = new Person("Masha");
    int songs[] = {1, 2, 3};
    person->appendLikedSongs(songs, 3);
    person->printInfo();
    delete person;
}
