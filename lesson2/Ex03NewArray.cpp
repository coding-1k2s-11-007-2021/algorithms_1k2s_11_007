#include <iostream>


int main() {
    using namespace std;

    int arrLen;
    cin >> arrLen;
    int* a = new int[arrLen];
    cout << "Array address: " << a << endl;
    for (int i = 0; i < arrLen; i++) {
        cin >> a[i];
    }
    for (int i = 0; i < arrLen; i++) {
        cout << a[i] << " ";
    }
    cout << endl;

    cin >> arrLen;
    delete [] a; // solution
    a = new int[arrLen]; // What's wrong with this line???
    for (int i = 0; i < arrLen; i++) {
        cout << *(a + i) << " ";
    }
    cout << "Array address: " << a << endl;
    for (int i = 0; i < arrLen; i++) {
        cin >> *(a + i);
    }
    for (int i = 0; i < arrLen; i++) {
        cout << *(a + i) << " ";
    }
    cout << endl;
    
    return 0;
}
