#include <iostream>


int main() {
    using namespace std;
    int a = 5;
    int* aPointer = &a;
    cout << *aPointer << endl;
    float* aFloatPointer = (float*)aPointer;
    cout << *aFloatPointer << endl;
    return 0;
}
