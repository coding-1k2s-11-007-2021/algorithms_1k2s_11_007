#include <iostream>


int main() {
    using namespace std;
    int a = 5;
    int b = 1;
    cout << a + ++b << endl;
    cout << b << endl;
    b = 1;
    cout << a + b++ << endl;
    cout << b << endl;
    return 0;
}
