#include <iostream>


int main() {
    using namespace std;
    int a[] = {1, 2, 3};
    cout << a[0] << endl;
    cout << *a << endl;
    cout << a[1] << endl;
    cout << *(a + 1) << endl;
    return 0;
}
