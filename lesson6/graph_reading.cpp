#include <iostream>
#include <vector>
#include <tuple>

std::vector<int>** edgeMatrixToAdjacencyList(int** edgeMatrix, int rowCount, int columnCount, int vertexCount) {
    std::vector<int>** adjacencyList = new std::vector<int>*[vertexCount];
    for (int i = 0; i < vertexCount; i++) {
        adjacencyList[i] = new std::vector<int>;
    }

    for (int i = 0; i < rowCount; i++) {
        adjacencyList[edgeMatrix[i][0] - 1]->push_back(edgeMatrix[i][1] - 1);
        // In case the graph is undirected we need to store a link back
        adjacencyList[edgeMatrix[i][1] - 1]->push_back(edgeMatrix[i][0] - 1);
    }

    return adjacencyList;
}

void printAdjacencyList(std::vector<int>** adjacencyList, int vertexCount) {
    for (int i = 0; i < vertexCount; i++) {
        std::cout << i+1 << "-> ";
        for (int j = 0; j < adjacencyList[i]->size(); j++) {
            int currentElement = adjacencyList[i]->at(j) + 1;
            std::cout << currentElement << ", ";
        }
        std::cout << std::endl;
    }
}

int main() {
    int vertexCount;
    std::cin >> vertexCount;

    int rowCount;
    int columnCount;
    std::cin >> rowCount;
    std::cin >> columnCount;

    int** edgeMatrix = new int*[rowCount];
    for (int i = 0; i < rowCount; i++) {
        edgeMatrix[i] = new int[columnCount];
        for (int j = 0; j < columnCount; j++) {
            std::cin >> edgeMatrix[i][j];
        }
    }

    std::vector<int>** adjacencyList = edgeMatrixToAdjacencyList(edgeMatrix, rowCount, columnCount, vertexCount);
    printAdjacencyList(adjacencyList, vertexCount);

    for (int i = 0; i < rowCount; i++) {
        delete[] edgeMatrix[i];
    }
    delete[] edgeMatrix;

    for (int i = 0; i < vertexCount; i++) {
        delete adjacencyList[i];
    }
    delete[] adjacencyList;

    return 0;
}

/*

INPUT EXAMPLE:
7
8 2 1 2 2 3 3 1 2 4 2 5 2 7 4 6 5 6

OUTPUT EXAMPLE:
1-> 2, 3, 
2-> 1, 3, 4, 5, 7, 
3-> 2, 1, 
4-> 2, 6, 
5-> 2, 6, 
6-> 4, 5, 
7-> 2,

*/
