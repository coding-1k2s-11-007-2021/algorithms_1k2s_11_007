#include <iostream>


int bin_search(int* arr, int arraySize, int targetElement) {
    int left = 0;
    int right = arraySize - 1;

    while (left <= right) {
        int middle = (left + right) / 2;
        if (arr[middle] == targetElement) {
            return middle;
        } else if (arr[middle] < targetElement) {
            left = middle + 1;
        } else {
            right = middle - 1;
        }
    }

    return -1;
}

int main() {
    int a[] = {1, 3, 4, 7, 10, 16};
    int arraySize = 6;

    int targetElement = -1;

    int result = bin_search(a, arraySize, targetElement);

    std::cout << result << std::endl;

    return 0;
}
