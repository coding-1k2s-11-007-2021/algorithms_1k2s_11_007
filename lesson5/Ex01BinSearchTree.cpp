#include <cstddef>
#include <iostream>
#include <string>
#include <queue>


class BinarySearchTree {
    private:
        int value;
        BinarySearchTree* left;
        BinarySearchTree* right;

    public:
        BinarySearchTree(int value, BinarySearchTree* left = nullptr, BinarySearchTree* right = nullptr) {
            this->value = value;
            this->left = left;
            this->right = right;
        }

        int getValue() {
            return this->value;
        }

        void setValue(int value) {
            this->value = value;
        }

        BinarySearchTree* getLeft() {
            return this->left;
        }

        void setLeft(BinarySearchTree* left) {
            this->left = left;
        }

        BinarySearchTree* getRight() {
            return this->right;
        }

        void setRight(BinarySearchTree* right) {
            this->right = right;
        }

        int getHeight() {
            int leftHeight = this->left != nullptr ? this->left->getHeight() : 0;
            int rightHeight = this->right != nullptr ? this->right->getHeight() : 0;
            return std::max(leftHeight, rightHeight) + 1;
        }

        std::string getString() {
            std::string leftInorder = this->left != nullptr ? this->left->getString() : "";
            std::string rightInorder = this->right != nullptr ? this->right->getString() : "";
            if (leftInorder.length() > 0) {
                leftInorder = "(" + leftInorder + ") ";
            }
            if (rightInorder.length() > 0) {
                rightInorder = " (" + rightInorder + ")";
            }
            return leftInorder + std::to_string(this->value) + rightInorder;
        }
};

BinarySearchTree* buildTree() {
    std::queue<BinarySearchTree*> currentQueue;
    BinarySearchTree* currentNode = nullptr;
    int numberOfNums;
    int currentNumber;

    std::cin >> numberOfNums;
    std::cin >> currentNumber;

    BinarySearchTree* root = new BinarySearchTree(currentNumber);
    currentQueue.push(root);

    for (int i = 1; i < numberOfNums; i++) {
        std::cin >> currentNumber;
        if (i % 2 == 1) {
            currentNode = currentQueue.front();
            currentQueue.pop();
        }
        if (currentNumber != -1) {
            BinarySearchTree* newElement = new BinarySearchTree(currentNumber);
            if (i % 2 == 1) {
                currentNode->setLeft(newElement);
            } else {
                currentNode->setRight(newElement);
            }
            currentQueue.push(newElement);
        }
    }
    return root;
}

int main() {
    BinarySearchTree* tree = buildTree();
    std::cout << tree->getString() << std::endl;
    std::cout << tree->getHeight() << std::endl;
}
