#pragma once
#include "ListInterface.hpp"


template <typename T>
class ArrayList : public ListInterface<T> {
private:
	T* arr;
	int size;
	int capacity;

	void extend();

public:
	ArrayList();
	ArrayList(T* initArr, int initArrSize);
	~ArrayList();

	void append(T x);
	void printList();
};
