#pragma once


template <typename T>
class ListInterface {
public:
	virtual void append(T x) = 0;
	virtual void printList() = 0;
};
