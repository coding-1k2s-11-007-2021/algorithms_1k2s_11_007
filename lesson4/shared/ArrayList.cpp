#include <iostream>
#include <string>
#include "ArrayList.hpp"


template <typename T>
ArrayList<T>::ArrayList() {
    this->size = 0;
    this->capacity = 10;
    this->arr = new T[this->capacity];
}


template <typename T>
ArrayList<T>::ArrayList(T* initArr, int initArrSize) {
    this->size = initArrSize;
    this->capacity = initArrSize * 2;
    this->arr = new T[this->capacity];
    for (int i = 0; i < this->size; i++) {
        this->arr[i] = initArr[i];
    }
}

template <typename T>
void ArrayList<T>::extend() {
	if (this->size == this->capacity) {
        this->capacity *= 2;
        T* newArr = new T[this->capacity];
        for (int i = 0; i < this->size; i++) {
            newArr[i] = this->arr[i];
        }
        delete [] this->arr;
        this->arr = newArr;
    }
}

template <typename T>
void ArrayList<T>::append(T x) {
	this->extend();
    this->size++;
    this->arr[size - 1] = x;
}

template <typename T>
void ArrayList<T>::printList() {
	using namespace std;
    for (int i = 0; i < this->size; i++) {
        cout << this->arr[i] << " ";
    }
    cout << endl;
}

template <typename T>
ArrayList<T>::~ArrayList() {
    delete [] this->arr;
}

template class ArrayList<std::string>;
