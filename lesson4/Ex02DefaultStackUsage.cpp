#include <iostream>
#include <stack>


int main() {
	using namespace std;

	stack<int> s;
    s.push(1);
    s.push(2);
    s.push(3);
    while (!s.empty()) {
        cout << s.top() << endl;
        s.pop();
    }

	return 0;
}
