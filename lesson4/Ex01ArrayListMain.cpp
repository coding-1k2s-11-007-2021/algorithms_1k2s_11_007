#include <iostream>
#include <string>
#include "shared/ArrayList.hpp"


int main() {
	using namespace std;

	string initArr[2] = {"hello", "there"};
	ArrayList<string> arrayList(initArr, 2);
	arrayList.printList();
	arrayList.append("it's");
	arrayList.append("working code!!!");
	arrayList.printList();

	return 0;
}
