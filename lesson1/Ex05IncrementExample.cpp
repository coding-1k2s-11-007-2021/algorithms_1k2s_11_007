#include <iostream>


void increment(int* a) {
    *a = *a + 1;
}

void increment2(int& a) {
    a += 1;
}


int main() {
    using namespace std;
    int a = 5;
    cout << a << endl;
    increment(&a);
    cout << a << endl;
    increment2(a);
    cout << a << endl;
    int& b = a;
    b += 1;
    cout << a << endl;
    return 0;
}
