#include <iostream>



int main() {
    using namespace std;
    int a = 5;
    int* aPointer = &a;
    cout << a << endl;
    cout << aPointer << endl;
    cout << *aPointer << endl;
    return 0;
}
