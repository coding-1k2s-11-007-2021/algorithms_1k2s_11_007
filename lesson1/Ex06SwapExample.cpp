#include <iostream>


void swap(int* a, int* b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap2(int& a, int& b) {
    int tmp = a;
    a = b;
    b = tmp;
}


int main() {
    using namespace std;
    int a = 5;
    int b = 6;
    cout << a << " " << b << endl;
    swap(&a, &b);
    cout << a << " " << b << endl;
    swap2(a, b);
    cout << a << " " << b << endl;
    return 0;
}
