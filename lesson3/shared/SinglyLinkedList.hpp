#pragma once
#include "ListInterface.hpp"


class ListNode {
private:
	int value;
	ListNode* next;
};


class SinglyLinkedList : public ListInterface {
private:
	ListNode* head;

public:
	SinglyLinkedList(int* initArr, int initArrSize, bool isReverse);

	void append(int x);
};
