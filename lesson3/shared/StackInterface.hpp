#pragma once


class StackInterface {
public:
    virtual void push(int x) = 0;   
};
