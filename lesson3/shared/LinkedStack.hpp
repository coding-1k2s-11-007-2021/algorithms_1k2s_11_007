#pragma once
#include "StackInterface.hpp"
#include "SinglyLinkedList.hpp"


class LinkedStack : public StackInterface {
private:
    SinglyLinkedList* lst;

public:
    LinkedStack();

    void push(int x);
};
