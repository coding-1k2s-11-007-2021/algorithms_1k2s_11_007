#pragma once
#include "ListInterface.hpp"


class ArrayList : public ListInterface {
private:
	int* arr;

public:
	ArrayList(int* initArr, int initArrSize, bool isReverse);

	void append(int x);
};
